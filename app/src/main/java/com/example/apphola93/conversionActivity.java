package com.example.apphola93;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.google.android.material.textfield.TextInputEditText;

public class conversionActivity extends AppCompatActivity {
    private TextInputEditText txtGrados;

    private RadioGroup grupoGrados;

    private RadioButton btnCF, btnFC;

    private TextView txtResultado;

    private Button btnCalcular, btnLimpiar, btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_conversion);
        iniciarComponentes();
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (grupoGrados.getCheckedRadioButtonId() == -1 || txtGrados.getText().toString().matches("")){
                    Toast.makeText(getApplicationContext(),"Faltaron campos por marcar / rellenar", Toast.LENGTH_SHORT).show();
                } else if (btnCF.isChecked()) {
                    float C1 = Float.parseFloat(txtGrados.getText().toString());
                    float F1 = ((((float) 9/5)*C1)+32);
                    txtResultado.setText(String.format("El resultado es: %.2f °F", F1));
                } else if (btnFC.isChecked()) {
                    float F2 = Float.parseFloat(txtGrados.getText().toString());
                    float C2 = ((F2 - 32)*((float) 5/9));
                    txtResultado.setText(String.format("El resultado es: %.2f °C", C2));
                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtGrados.setText("");
                btnCF.setChecked(false);
                btnFC.setChecked(false);
                txtResultado.setText("");
            }
        });
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    public void iniciarComponentes(){
        txtGrados = (TextInputEditText) findViewById(R.id.txtGrados);
        txtResultado = (TextView) findViewById(R.id.txtResultado);
        grupoGrados = (RadioGroup) findViewById(R.id.grupoGrados);
        btnCF = (RadioButton) findViewById(R.id.btnCF);
        btnFC = (RadioButton) findViewById(R.id.btnFC);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
    }
}