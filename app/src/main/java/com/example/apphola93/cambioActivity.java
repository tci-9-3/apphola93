package com.example.apphola93;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.google.android.material.textfield.TextInputEditText;

public class cambioActivity extends AppCompatActivity {
    private TextInputEditText txtDinero;

    private TextView txtResultado;

    private Spinner conv_spinner;

    private Button btnCalcular, btnLimpiar, btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_cambio);
        iniciarComponentes();
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = conv_spinner.getSelectedItem().toString();
                if (txtDinero.getText().toString().matches("") || value.matches("Convertir a:")){
                    Toast.makeText(getApplicationContext(),"Ingresa una cantidad o elija una opcion válida de conversión", Toast.LENGTH_SHORT).show();
                }else if (value.matches("Dólares americanos")){
                    float dinero = Float.parseFloat(txtDinero.getText().toString());
                    float usd = 16.739F;
                    float res = dinero/usd;
                    txtResultado.setText(String.format("El resultado es: $%.2f Dólares americanos", res));
                }
                else if (value.matches("Euros")){
                    float dinero = Float.parseFloat(txtDinero.getText().toString());
                    float eur = 18.09F;
                    float res = dinero/eur;
                    txtResultado.setText(String.format("El resultado es: €%.2f Euros", res));
                }
                else if (value.matches("Dólar canadiense")){
                    float dinero = Float.parseFloat(txtDinero.getText().toString());
                    float cad = 12.185F;
                    float res = dinero/cad;
                    txtResultado.setText(String.format("El resultado es: C$%.2f Dólares canadienses", res));
                }
                else if (value.matches("Libra esterlina")){
                    float dinero = Float.parseFloat(txtDinero.getText().toString());
                    float gbp = 21.24F;
                    float res = dinero/gbp;
                    txtResultado.setText(String.format("El resultado es: £%.2f Libras esterlinas", res));
                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtDinero.setText("");
                txtResultado.setText("");
                conv_spinner.setSelection(0);
            }
        });
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    public void iniciarComponentes(){
        txtDinero = (TextInputEditText) findViewById(R.id.txtDinero);
        txtResultado = (TextView) findViewById(R.id.txtResultado);
        conv_spinner = (Spinner) findViewById(R.id.conv_spinner);
        btnCalcular = (Button)  findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
    }
}